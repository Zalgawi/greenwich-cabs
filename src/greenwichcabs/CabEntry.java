
package greenwichcabs;

public class CabEntry {
        private int driverID;
        private String journeyTime;
        private String pickupLocation;
        private String dropoffLocation;
        private String passengerName;
        private double totalFare;
        private double tipFare;
        private String accountName;
        private String telephoneNumber;

        
    public CabEntry(){
        
    }
    
    public CabEntry(int driverId, String journeyTime, String pickupLocation,
            String dropoffLocation,String passengerName, double totalFare, 
            double tipFare, String accountName, String telephoneNumber )
    {
        this.driverID = driverId;
        this.journeyTime = journeyTime;
        this.pickupLocation = pickupLocation;
        this.dropoffLocation = dropoffLocation;
        this.passengerName = passengerName;
        this.totalFare = totalFare;
        this.tipFare = tipFare;
        this.accountName = accountName;
        this.telephoneNumber = telephoneNumber;
        
    }

    public int getDriverID() 
    { return driverID; }

    public void setDriverID(int driverID) 
    { this.driverID = driverID; }

    public String getPickupLocation() 
    { return pickupLocation; }

    public void setPickupLocation(String pickupLocation) 
    { this.pickupLocation = pickupLocation; }

    public String getDropoffLocation() 
    { return dropoffLocation; }

    public void setDropoffLocation(String dropoffLocation) 
    { this.dropoffLocation = dropoffLocation; }

    public String getPassengerName() 
    { return passengerName; }

    public void setPassengerName(String passengerName) 
    { this.passengerName = passengerName; }

    public double getTotalFare() 
    { return totalFare; }

    public void setTotalFare(double totalFare) 
    { this.totalFare = totalFare; }

    public double getTipFare() 
    { return tipFare; }

    public void setTipFare(double tipFare) 
    { this.tipFare = tipFare; }

    public String getAccountName() 
    { return accountName; }

    public void setAccountName(String accountName) 
    { this.accountName = accountName; }

    public String getTelephoneNumber() 
    { return telephoneNumber; }

    public void setTelephoneNumber(String telephoneNumber) 
    { this.telephoneNumber = telephoneNumber; }

    public String getJourneyTime() 
    { return journeyTime; }

    public void setJourneyTime(String journeyTime) 
    { this.journeyTime = journeyTime; }
}