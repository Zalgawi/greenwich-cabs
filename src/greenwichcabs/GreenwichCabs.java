/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package greenwichcabs;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Zayda
 */
public class GreenwichCabs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        JFrame frame = new JFrame();
       
        
        GreenwichCabsUI greenwichPanel = new GreenwichCabsUI();

        Utility.TableMethods tableLoader = new Utility.TableMethods();
        
        DefaultTableModel model = tableLoader.loadJourneyTable();
        
        JTable entryTable = new JTable(model);
        entryTable.setBounds(30, 40, 200, 300);
        
        
        entryTable.setFillsViewportHeight(true);
        entryTable.setAutoCreateRowSorter(true);
        
        greenwichPanel.setJTable(entryTable);
        
        greenwichPanel.setVisible(true);
        
        frame.add(greenwichPanel);
        frame.setVisible(true);
        frame.pack();
        
        
    }
    
}
