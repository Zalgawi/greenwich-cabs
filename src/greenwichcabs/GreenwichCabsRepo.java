package greenwichcabs;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

public class GreenwichCabsRepo {
   
    
    public static java.sql.Connection getConnection() throws Exception {
        try {
            String driver = "com.mysql.cj.jdbc.Driver";
            String url = "jdbc:mysql://127.0.0.1:3306/greenwichcabsdb";
            String username = "root";
            String password = "root";
            Class.forName(driver);
            
            java.sql.Connection conn = DriverManager.getConnection(url,username, password);
            System.out.println("Connection Successful");
            return conn;
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    } 
    
    
    public static void insertTable(final int driverId,final Date journeyTime,
            final String pickupLocation, final String dropoffLocation,
            final String passengerName,final double totalFare, 
            final double tipFare,final String accountName,
            final String telephoneNumber ) throws Exception{
        try {
            java.sql.Connection con = getConnection();
            
            String insert = "INSERT INTO cabjourneytable(driverId, journeyTime,"
                    + " pickupLocation, dropoffLocation, passengerName, "
                    + "totalFare, tipFare, accountName, telephoneNumber) "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement ps = con.prepareStatement(insert);
            Timestamp convertedJourneyTime = new java.sql.Timestamp
            (journeyTime.getTime());
            
            ps.setInt(1, driverId);
            ps.setTimestamp(2, new java.sql.Timestamp(convertedJourneyTime.getTime()));
            ps.setString(3, pickupLocation);
            ps.setString(4, dropoffLocation);
            ps.setString(5, passengerName);
            ps.setDouble(6, totalFare);
            ps.setDouble(7, tipFare);
            ps.setString(8, accountName);
            ps.setString(9, telephoneNumber);

             ps.executeUpdate();         
            } 
        catch(Exception e) 
        {
         System.out.println(e);
        }
        finally 
        {
         System.out.println("Insert Completed.");
        }
    }
}
    



