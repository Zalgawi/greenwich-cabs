/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package greenwichcabs;

import static greenwichcabs.GreenwichCabsUI.getConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Zayda
 */
public class Utility {
    
    public static class TableMethods{
        
       
    
    public DefaultTableModel loadJourneyTable() throws Exception {
        
        GreenwichCabsRepo repo = new GreenwichCabsRepo();  
        ArrayList<CabEntry> entryList = new ArrayList<CabEntry>();
        
        try {
            java.sql.Connection con = getConnection();
           
            String select = "SELECT * From cabjourneytable";
           
            PreparedStatement pull = con.prepareStatement(select);
            
            ResultSet rs = pull.executeQuery(select);
            
            while(rs.next()){
               CabEntry newEntry = new CabEntry(); 
               newEntry.setDriverID(rs.getInt("driverId"));
               newEntry.setJourneyTime(rs.getString("journeyTime"));
               newEntry.setPickupLocation(rs.getString("pickupLocation"));
               newEntry.setDropoffLocation(rs.getString("dropoffLocation"));
               newEntry.setPassengerName(rs.getString("passengerName"));
               newEntry.setTotalFare(rs.getDouble("totalFare"));
               newEntry.setTipFare(Double.parseDouble(rs.getString("tipFare")));
               newEntry.setAccountName(rs.getString("accountName"));
               newEntry.setTelephoneNumber(rs.getString("telephoneNumber"));
               
                /*
               
                set all values for CabEntry object
               */
               
               entryList.add(newEntry);
            }
            
            
            
        }
        catch (Exception e){
           JOptionPane.showMessageDialog(null, e); 
        }
        
    String col[] = {"driverId","journeyTime","pickupLocation", "dropoffLocation", "passengerName", "totalFare", "tipFare", "accountName", "telephoneNumber"};
    DefaultTableModel tableModel = new DefaultTableModel(col, 0);

    
    
    for(CabEntry e : entryList)
    {
        Object rowObject[] = new Object[9];
        
        rowObject[0] = e.getDriverID();
        rowObject[1] = e.getJourneyTime();
        rowObject[2] = e.getPickupLocation();
        rowObject[3] = e.getDropoffLocation();
        rowObject[4] = e.getPassengerName();
        rowObject[5] = e.getTotalFare();
        rowObject[6] = e.getTipFare();
        rowObject[7] = e.getAccountName();
        rowObject[8] = e.getTelephoneNumber();
        
        //finish filling rowObject
        
        tableModel.addRow(rowObject);
        
    }
    
    
    return tableModel;
   }
    
    
    }
    
}
